﻿using CDEVTEAM.Common;
using CDEVTEAM.Infrastructure.Configurations;
using CDEVTEAM.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace CDEVTEAM.Infrastructure
{
    public class CdevTeamDbContext : DbContext
    {
        private readonly AppConfiguration appConfiguration;
        public CdevTeamDbContext()
        {
            appConfiguration = new AppConfiguration();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(appConfiguration.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PermissionConfiguration());
            modelBuilder.ApplyConfiguration(new PermissionTypeConfiguration());

            AddMyFilters(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        //DBSETS
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionType> PermissionTypes { get; set; }

        private void AddMyFilters(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permission>().HasQueryFilter(c => c.IsDeleted == false);
            modelBuilder.Entity<PermissionType>().HasQueryFilter(c => c.IsDeleted == false);
        }
    }
}
