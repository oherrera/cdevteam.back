﻿using CDEVTEAM.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Infrastructure.Configurations
{
    class PermissionTypeConfiguration : IEntityTypeConfiguration<PermissionType>
    {
        public void Configure(EntityTypeBuilder<PermissionType> permissionTypeBuilder)
        {
            permissionTypeBuilder.ToTable("PermissionTypes");
            permissionTypeBuilder.HasKey(p => p.Id);
            permissionTypeBuilder.Property(p => p.Id).ValueGeneratedOnAdd();
            permissionTypeBuilder.Property(p => p.Description).HasMaxLength(150).IsRequired();

            permissionTypeBuilder.HasData(
                new PermissionType { Id = 1, Description = "Enfermedad", IsDeleted = false },
                new PermissionType { Id = 2, Description = "Diligencias", IsDeleted = false },
                new PermissionType { Id = 3, Description = "Otros", IsDeleted = false }
            );



        }
    }
}
