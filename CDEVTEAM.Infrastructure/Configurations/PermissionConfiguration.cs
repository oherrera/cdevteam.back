﻿using CDEVTEAM.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CDEVTEAM.Infrastructure.Configurations
{
    public class PermissionConfiguration : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> permissionBuilder)
        {
            permissionBuilder.ToTable("Permissions");
            permissionBuilder.HasKey(p => p.Id);
            permissionBuilder.Property(p => p.Id).ValueGeneratedOnAdd(); ;
            permissionBuilder.Property(p => p.EmployeeName).HasMaxLength(60).IsRequired();
            permissionBuilder.Property(p => p.EmployeeLastName).HasMaxLength(120).IsRequired();
            permissionBuilder.Property(p => p.PermissionTypeId).IsRequired();
            permissionBuilder.Property(p => p.PermissionDate).IsRequired();

            permissionBuilder.HasOne(p => p.PermissionType)
                .WithMany(pt => pt.Permissions)
                .HasForeignKey(pt => pt.PermissionTypeId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
