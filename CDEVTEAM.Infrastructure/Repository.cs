﻿using CDEVTEAM.Models.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CDEVTEAM.Infrastructure
{
    public class Repository<TEntity> where TEntity : class
    {
        protected readonly CdevTeamDbContext _context;
        IDbContextTransaction transaction;
        public Repository(CdevTeamDbContext context)
        {
            _context = context;
        }
        public void BeginTransaction()
        {
            transaction = _context.Database.BeginTransaction();
        }

        public async Task<TEntity> GetEntity(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> GetAllEntities()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> Search(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public async Task<TEntity> SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().SingleOrDefaultAsync(predicate);
        }

        public async Task<TEntity> FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(predicate);
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().AnyAsync(predicate);
        }

        public void AddEntity(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void AddEntities(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);
        }

        public void RemoveEntity(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void RemoveEntities(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public void DeleteEntity(TEntity entity)
        {
            if (entity is ISoftDeleted deleted)
            {
                deleted.IsDeleted = true;

                _context.Set<TEntity>().Attach(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                _context.Set<TEntity>().Remove(entity);
            }
        }

        public async Task<int> Count(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().CountAsync(predicate);
        }

        public void SetEntityState(TEntity e, EntityState entityState)
        {
            _context.Entry(e).State = entityState;
        }

        public async Task<int> SaveChanges()
        {
            return await _context.SaveChangesAsync();
        }

        public void CommitTransaction()
        {
            transaction.Commit();
        }
        public void RollBackTransaction()
        {
            transaction.Rollback();
        }
    }
}
