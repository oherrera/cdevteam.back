﻿using CDEVTEAM.Common;
using NUnit.Framework;


namespace CDEVTEAM.UnitTests
{
    [TestFixture]
    class CdevTeam_Common_DbConnection
    {
        private AppConfiguration _appConfiguration;
        [SetUp]
        public void Setup()
        {
            _appConfiguration = new AppConfiguration();
        }

        [TestCase]
        public void ConnectionString_NullOrEmpty_ReturnFalse()
        {
            //Arrange
            var connection = _appConfiguration.ConnectionString;

            //Act 
            var isNullOrEmpty = string.IsNullOrEmpty(connection);

            //Assert
            Assert.IsFalse(isNullOrEmpty);
        }
    }
}
