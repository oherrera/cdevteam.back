using CDEVTEAM.ServiceData.Test;
using NUnit.Framework;

namespace CDEVTEAM.UnitTests
{
    public class CdevTeam_Service_DbConnection
    {
        ConnectDatabaseService connectDatabaseService;

        [SetUp]
        public void Setup()
        {
            connectDatabaseService = new ConnectDatabaseService();
        }

        [TestCase]
        public void ConnectionToDataBase_ConnectionOk_ReturnTrue()
        {
            //Act
            var canConnect = connectDatabaseService.TestConnection();

            //Assert
            Assert.IsTrue(canConnect);
        }
    }
}