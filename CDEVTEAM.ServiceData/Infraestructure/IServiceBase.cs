﻿using CDEVTEAM.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CDEVTEAM.ServiceData.Infraestructure
{
    public interface IServiceBase<T> where T : class
    {
        Task<ServiceResponse> Add(T entity);
        Task<ServiceResponse> Update(int id, T entity);
        Task<ServiceResponse> Delete(int id);
        Task<T> Get(int id);
        Task<IEnumerable<T>> GetAll();
    }
}
