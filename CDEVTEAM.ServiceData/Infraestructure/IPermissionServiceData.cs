﻿using CDEVTEAM.Models.Domain;
using CDEVTEAM.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CDEVTEAM.ServiceData.Infraestructure
{
    public interface IPermissionServiceData : IServiceBase<Permission>
    {
        //Add specific methods that are not in the repository
        Task<IEnumerable<PermissionForGrid>> GetAllForGrid();
    }
}
