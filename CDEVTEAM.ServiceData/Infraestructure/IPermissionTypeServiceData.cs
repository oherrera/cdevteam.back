﻿using CDEVTEAM.Models.Domain;
using CDEVTEAM.Models.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CDEVTEAM.ServiceData.Infraestructure
{
    public interface IPermissionTypeServiceData : IServiceBase<PermissionType>
    {
        //Add specific methods that are not in the repository
        Task<IEnumerable<PermissionTypeForDrop>> GetAllForDrop();
    }
}
