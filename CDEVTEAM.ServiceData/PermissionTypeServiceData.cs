﻿using CDEVTEAM.Common;
using CDEVTEAM.Infrastructure;
using CDEVTEAM.Models.Domain;
using CDEVTEAM.Models.Dto;
using CDEVTEAM.ServiceData.Infraestructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDEVTEAM.ServiceData
{
    public class PermissionTypeServiceData : Repository<PermissionType>, IPermissionTypeServiceData
    {
        public PermissionTypeServiceData(CdevTeamDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<PermissionTypeForDrop>> GetAllForDrop()
        {
            try
            {
                var permissionTypesForGrid = await _context.PermissionTypes
                    .Select(pt => new PermissionTypeForDrop
                    {
                        Id = pt.Id,
                        Description = pt.Description
                    }).ToListAsync();

                return permissionTypesForGrid;
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed("An error ocurred trying to get permission types", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
        }

        //METHODS ARE NOT REQUIRED YET

        public Task<ServiceResponse> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<PermissionType> Get(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PermissionType>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResponse> Update(int id, PermissionType entity)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResponse> Add(PermissionType entity)
        {
            throw new NotImplementedException();
        }
    }
}
