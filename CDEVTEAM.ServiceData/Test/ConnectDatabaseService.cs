﻿using CDEVTEAM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.ServiceData.Test
{
    public class ConnectDatabaseService
    {
        private readonly CdevTeamDbContext _context;
        public ConnectDatabaseService()
        {
            _context = new CdevTeamDbContext();
        }

        public bool TestConnection()
        {
            return _context.Database.CanConnect();
        }
    }
}
