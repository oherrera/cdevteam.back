﻿using CDEVTEAM.Common;
using CDEVTEAM.Infrastructure;
using CDEVTEAM.Models.Domain;
using CDEVTEAM.Models.Dto;
using CDEVTEAM.ServiceData.Infraestructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDEVTEAM.ServiceData
{
    public class PermissionServiceData : Repository<Permission>, IPermissionServiceData
    {
        public PermissionServiceData(CdevTeamDbContext context) : base(context)
        {
        }

        public async Task<ServiceResponse> Add(Permission entity)
        {
            var sr = new ServiceResponse();

            try
            {
                AddEntity(entity);
                await SaveChanges();
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("An error occurred trying to add this permission", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }

            return sr;
        }

        public async Task<ServiceResponse> Delete(int id)
        {
            var sr = new ServiceResponse();
            try
            {
                var permissionToDelete = await SingleOrDefault(p => p.Id == id);

                if (permissionToDelete == null)
                    return new ServiceEntityNotFound();

                DeleteEntity(permissionToDelete);
                await SaveChanges();
            }
            catch (ArgumentNullException e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("There are not entity to look for", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
            catch (InvalidOperationException e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("An error occurred trying to delete this permission", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("An error occurred trying to delete this permission", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }

            return sr;
        }

        public async Task<Permission> Get(int id)
        {
            try
            {
                var permission = await GetEntity(id);

                if (permission == null)
                    throw new ServiceException("Requested entity not found, operation could not be performed");

                return permission;

            }
            catch (ServiceException e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed(e.Message, e, GetType().Name, methodName);
                throw e;
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed("An error occurred trying to get the requested permission", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
        }

        public async Task<IEnumerable<Permission>> GetAll()
        {
            try
            {
                var permissions = await GetAllEntities();
                return permissions;
            }
            catch (ArgumentNullException e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed("There are not entity to look for", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed("An error ocurred trying to get permissions", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
        }

        public async Task<IEnumerable<PermissionForGrid>> GetAllForGrid()
        {
            try
            {
                var permissionForGrid = await _context.Permissions
                    .Select(p => new PermissionForGrid
                    {
                        Id = p.Id,
                        EmployeeName = p.EmployeeName,
                        EmployeeLastName = p.EmployeeLastName,
                        PermisionTypeDescription = p.PermissionType.Description,
                        PermissionDate = p.PermissionDate.ToString("yyyy-MM-dd")
                    }).ToListAsync();

                return permissionForGrid;
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed("An error ocurred trying to get permissions", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
        }

        public async Task<ServiceResponse> Update(int id, Permission permissionNewValues)
        {
            var sr = new ServiceResponse();

            try
            {
                var permissionToUpdate = await GetEntity(id);

                if(permissionToUpdate == null)
                    return new ServiceEntityNotFound();

                permissionToUpdate.EmployeeName = permissionNewValues.EmployeeName;
                permissionToUpdate.EmployeeLastName = permissionNewValues.EmployeeLastName;
                permissionToUpdate.PermissionTypeId = permissionNewValues.PermissionTypeId;
                permissionToUpdate.PermissionDate = permissionNewValues.PermissionDate;
   
                await SaveChanges();
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("An error ocurred trying to update the selected permission", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }

            return sr;
        }
    }
}
