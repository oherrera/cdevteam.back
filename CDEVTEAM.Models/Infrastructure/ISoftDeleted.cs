﻿
namespace CDEVTEAM.Models.Infrastructure
{
    public interface ISoftDeleted
    {
        public bool IsDeleted { get; set; }
    }
}
