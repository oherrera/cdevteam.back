﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Models.Dto
{
    public class PermissionTypeForDrop
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
