﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Models.Dto
{
    public class PermissionForGrid
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeLastName { get; set; }

        public string PermisionTypeDescription { get; set; }
        public string PermissionDate { get; set; }


    }
}
