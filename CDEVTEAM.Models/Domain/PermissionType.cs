﻿using CDEVTEAM.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Models.Domain
{
    public class PermissionType : ISoftDeleted
    {
        public int Id { get; set; }
        public string Description { get; set; }


        #region Composition Properties
        public bool IsDeleted { get; set; }
        #endregion

        #region Agregation Properties
        public ICollection<Permission> Permissions { get; set; }
        #endregion

    }
}
