﻿using CDEVTEAM.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Models.Domain
{
    public class Permission : ISoftDeleted
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeLastName { get; set; }
        public int PermissionTypeId { get; set; }
        public DateTime PermissionDate { get; set; }

        #region Composition Properties
        public bool IsDeleted { get; set; }
        #endregion

        #region Agregation Properties
        public PermissionType PermissionType { get; set; }
        #endregion
    }
}
