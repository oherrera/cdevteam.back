﻿using CDEVTEAM.Api.Helpers;
using CDEVTEAM.Common;
using CDEVTEAM.Models.Dto;
using CDEVTEAM.ServiceData.Infraestructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDEVTEAM.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class PermissionTypeController : ControllerBase
    {

        private readonly IPermissionTypeServiceData _permissionTypeServiceData;
        private readonly ControllerOperationFailed controllerFailedReporter;

        public PermissionTypeController(IPermissionTypeServiceData permissionTypeServiceData)
        {
            _permissionTypeServiceData = permissionTypeServiceData;
            controllerFailedReporter = new ControllerOperationFailed();
        }


        [HttpGet]
        [Route("drop/permission-types")]
        public async Task<PayLoad<IEnumerable<PermissionTypeForDrop>>> GetPermissionTypesForDrop()
        {
            var payLoad = new PayLoad<IEnumerable<PermissionTypeForDrop>>();
            try
            {
                var permissionTypes = await _permissionTypeServiceData.GetAllForDrop();
                payLoad.Data = permissionTypes;
            }
            catch (ServiceException e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = "An error occurred trying to get permission types, try again later or contact support department";
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                controllerFailedReporter.RegisterFailedOperation(payLoad.Message, e, GetType().Name, methodName);
            }

            return payLoad;
        }
    }
}
