﻿using AutoMapper;
using CDEVTEAM.Api.Helpers;
using CDEVTEAM.Api.ViewModels;
using CDEVTEAM.Common;
using CDEVTEAM.Models.Domain;
using CDEVTEAM.Models.Dto;
using CDEVTEAM.ServiceData.Infraestructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CDEVTEAM.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IPermissionServiceData _permissionServiceData;
        private readonly IMapper _mapper;

        private readonly ControllerOperationFailed controllerFailedReporter;

        public PermissionController(IPermissionServiceData permissionServiceData, IMapper mapper)
        {
            _permissionServiceData = permissionServiceData;
            _mapper = mapper;
            controllerFailedReporter = new ControllerOperationFailed();
        }


        [HttpGet]
        [Route("permissions")]
        public async Task<PayLoad<IEnumerable<PermissionForGrid>>> GetAllForGrid()
        {
            var payLoad = new PayLoad<IEnumerable<PermissionForGrid>>();
            try
            {
                var permissions = await _permissionServiceData.GetAllForGrid();
                payLoad.Data = permissions;
            }
            catch (ServiceException e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = "An error occurred trying to get permissions, try again later or contact support department";
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                controllerFailedReporter.RegisterFailedOperation(payLoad.Message, e, GetType().Name, methodName);
            }

            return payLoad;
        }

        [HttpGet]
        [Route("permission/{id}")]
        public async Task<PayLoad<PermissionViewModel>> GetPermission(int id)
        {
            var payLoad = new PayLoad<PermissionViewModel>();
            try
            {
                var permission = await _permissionServiceData.Get(id);
                var permissionViewModel = _mapper.Map<PermissionViewModel>(permission);
                payLoad.Data = permissionViewModel;
            }
            catch (ServiceException e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = "An error occurred trying to get permissions, try again later or contact support department";
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                controllerFailedReporter.RegisterFailedOperation(payLoad.Message, e, GetType().Name, methodName);
            }

            return payLoad;
        }

        [HttpPost]
        [Route("permission")]
        public async Task<PayLoad<PermissionViewModel>> Add([FromBody] PermissionViewModel permissionViewModel)
        {
            var payLoad = new PayLoad<PermissionViewModel>();
            try
            {
                if (!ModelState.IsValid)
                {
                    payLoad.Message = "The validations did not pass, check the captured information";
                    payLoad.Code = (int)HttpResponseCode.BadRequest;
                    payLoad.Data = null;
                }
                else
                {
                    var newPermission = _mapper.Map<Permission>(permissionViewModel);
                    var sr = await _permissionServiceData.Add(newPermission);

                    if (sr.Success)
                    {
                        permissionViewModel.Id = newPermission.Id;
                        payLoad.Data = permissionViewModel;
                    }

                    payLoad.Code = ServiceResponseResult.GetPayLoadCode(sr);
                    payLoad.Message = sr.UserMessage;
                }
            }
            catch (ServiceException e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = GlobalVariables.GeneralErrorMessage;
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                controllerFailedReporter.RegisterFailedOperation("An error occurred trying to add permission", e, GetType().Name, methodName);
            }
            return payLoad;
        }

        [HttpPut]
        [Route("permission/{id}")]
        public async Task<PayLoad<PermissionViewModel>> Update(int id, [FromBody] PermissionViewModel permissionViewModel)
        {
            var payLoad = new PayLoad<PermissionViewModel>();
            try
            {
                if (!ModelState.IsValid)
                {
                    payLoad.Message = "The validations did not pass, check the captured information";
                    payLoad.Code = (int)HttpResponseCode.BadRequest;
                    payLoad.Data = null;
                }
                else
                {
                    var permissionToUpdate = _mapper.Map<Permission>(permissionViewModel);
                    var sr = await _permissionServiceData.Update(id, permissionToUpdate);

                    if (sr.Success)
                        payLoad.Data = permissionViewModel;

                    payLoad.Code = ServiceResponseResult.GetPayLoadCode(sr);
                    payLoad.Message = sr.UserMessage;
                }
            }
            catch (ServiceException e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = GlobalVariables.GeneralErrorMessage;
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                controllerFailedReporter.RegisterFailedOperation("An error occurred trying to update permission", e, GetType().Name, methodName);
            }
            return payLoad;
        }

        [HttpDelete]
        [Route("permission/{id}")]
        public async Task<PayLoad<bool>> Delete(int id)
        {
            var payLoad = new PayLoad<bool>();
            try
            {
                var sr = await _permissionServiceData.Delete(id);

                if (sr.Success)
                    payLoad.Data = true;

                payLoad.Code = ServiceResponseResult.GetPayLoadCode(sr);
                payLoad.Message = sr.UserMessage;

            }
            catch (ServiceException e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.Code = (int)HttpResponseCode.ServerError;
                payLoad.Message = GlobalVariables.GeneralErrorMessage;
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                controllerFailedReporter.RegisterFailedOperation("An error occurred trying to delete permission", e, GetType().Name, methodName);
            }

            return payLoad;
        }
    }
}
