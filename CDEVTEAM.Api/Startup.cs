using AutoMapper;
using CDEVTEAM.Api.Config;
using CDEVTEAM.Common;
using CDEVTEAM.Infrastructure;
using CDEVTEAM.ServiceData;
using CDEVTEAM.ServiceData.Infraestructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Linq;

namespace CDEVTEAM.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Just for test enable all 
            services.AddCors(options =>
            {
                options.AddPolicy(name: "AllowedOrigins",
                                  builder =>
                                  {
                                      builder.AllowAnyOrigin()
                                      .AllowAnyHeader()
                                      .AllowAnyMethod();
                                  });
            });

            services.AddControllers();

            //Services
            services.AddScoped<IPermissionServiceData, PermissionServiceData>();
            services.AddScoped<IPermissionTypeServiceData, PermissionTypeServiceData>();

            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();

            //To use single instance to DB Context
            services.AddSingleton(dbContext => new CdevTeamDbContext());

            //To use single instance of autommaper
            services.AddSingleton(mapper);

            //OPEN API
            AddSwagger(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //To can use Index.html 
            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //to get wwwroot folder
            GlobalVariables.WebRoot = env.WebRootPath;


            app.UseCors("AllowedOrigins");
            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CDEVTEAM API V1");
            });
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            ApplyDataBaseMigrations(app);
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"CDEVTEAM API {groupName}",
                    Version = groupName,
                    Description = "CDEVTEAM API",
                    Contact = new OpenApiContact
                    {
                        Name = "CDEVTEAM Company",
                        Email = string.Empty,
                        Url = new Uri("https://cdevteam.com/"),
                    }
                });
            });
        }

        private void ApplyDataBaseMigrations(IApplicationBuilder builder)
        {
            var scope = builder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var ctx = scope.ServiceProvider.GetRequiredService<CdevTeamDbContext>();

            ctx.Database.Migrate();
        }
    }
}
