﻿using AutoMapper;
using CDEVTEAM.Api.ViewModels;
using CDEVTEAM.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDEVTEAM.Api.Config
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects

            //Domain to ViewModel
            CreateMap<Permission, PermissionViewModel>();

            //Viewmodel to Domain
            CreateMap<PermissionViewModel, Permission>();


        }
    }
}
