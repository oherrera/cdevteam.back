﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CDEVTEAM.Api.ViewModels
{
    public class PermissionViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The Employee Name is required")]
        [MaxLength(60, ErrorMessage = "The Employee Name must be less than {1} characters")]
        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "The Employee Lastname is required")]
        [MaxLength(120, ErrorMessage = "The Employee Lastname must be less than {1} characters")]
        public string EmployeeLastName { get; set; }

        [Required(ErrorMessage = "The Employee Permission is required")]
        public int PermissionTypeId { get; set; }

        [Required(ErrorMessage = "The Permission date is required")]
        public DateTime PermissionDate { get; set; }
    }
}
