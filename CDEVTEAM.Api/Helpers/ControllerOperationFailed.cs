﻿using CDEVTEAM.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDEVTEAM.Api.Helpers
{
    public class ControllerOperationFailed : ServiceOperationFailed
    {

        string EntityName { get; set; }
        string OperationName { get; set; }
        DateTime OperationFailedDate { get; set; }
        string InnerException { get; set; }
        private string Layer { get; set; }
        int LineCode { get; set; }
        public ControllerOperationFailed()
        {
            Layer = "controller";
        }

        public void RegisterFailedOperation(string userMessage, Exception exception, string serviceName = "", string operationName = "")
        {
            try
            {
                OperationFailedDate = DateTime.Now;
                Success = false;
                MessageType = "danger";
                UserMessage = userMessage;
                EntityName = serviceName;
                OperationName = operationName;
                SystemErrorMessage = exception.Message;
                int lineNumber = exception.GetLineNumber();

                if (exception.InnerException != null)
                    InnerException = exception.InnerException.Message;

                var webRoot = $"{GlobalVariables.WebRoot}/Logs";
                var fileName = Path.Combine(webRoot, "ErrorLogController.txt");
                if (!File.Exists(fileName))
                {
                    using (var streamWriter = new StreamWriter(fileName, true, Encoding.UTF8))
                        streamWriter.WriteLine($"DateTimeError;Entity;Operation;Layer;LineCode;Exception;InnnerException");
                }
                using (var streamWriter = new StreamWriter(fileName, true, Encoding.UTF8))
                {
                    streamWriter.WriteLine($"{OperationFailedDate:yyyy-MM-dd HH:mm:ss};{EntityName};{OperationName};{Layer};{lineNumber};{SystemErrorMessage};{InnerException}");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
