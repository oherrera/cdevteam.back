﻿using CDEVTEAM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDEVTEAM.Api.Helpers
{
    public static class ServiceResponseResult
    {
        public static int GetPayLoadCode(ServiceResponse sr)
        {
            if (sr.Success)
                return (int)HttpResponseCode.Ok;

            if (sr is ServiceEntityNotFound)
                return (int)HttpResponseCode.NotFound;

            if (sr is ServiceEntityDuplicated)
                return (int)HttpResponseCode.BadRequest;

            return (int)HttpResponseCode.ServerError;
        }
    }
}
