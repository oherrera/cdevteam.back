﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CDEVTEAM.Common
{
    public class AppConfiguration
    {
        private readonly ConfigurationBuilder configuration;


        //Configuration Properties
        private readonly string _ConnectionString = string.Empty;

        public AppConfiguration()
        {
            configuration = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configuration.AddJsonFile(path, false);

            var appsettings = configuration.Build();

            //SET PROPERTIES
            _ConnectionString = appsettings.GetConnectionString("DefaultConnection");
        }


        public string ConnectionString => _ConnectionString;
    }
}
