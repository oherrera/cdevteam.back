﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Common
{
    public class GlobalVariables
    {
        public static string WebRoot = string.Empty;

        public static string GeneralErrorMessage = "An error executing the operation, try again later or contact support department";
    }
}
