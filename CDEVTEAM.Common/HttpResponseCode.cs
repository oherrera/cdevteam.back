﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Common
{
    public enum HttpResponseCode
    {
        Ok = 200,
        Created = 201,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        ServerError = 500
    }
}
