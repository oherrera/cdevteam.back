﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDEVTEAM.Common
{
    public static class Extensions
    {
        public static int GetLineNumber(this Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                    return lineNumber;
            }

            return lineNumber;

        }
    }
}
