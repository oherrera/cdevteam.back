﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CDEVTEAM.Common
{
    public class ServiceResponse
    {
        public bool Success { get; set; }
        public string UserMessage { get; set; }
        public string MessageType { get; set; }
        public string SystemErrorMessage { get; set; }
        public List<string> ValidationErrors { get; set; }

        public ServiceResponse()
        {
            // By Default
            Success = true;
            UserMessage = "Operation ended successfully";
            MessageType = "success";
            ValidationErrors = new List<string>();
        }
    }

    public class ServiceEntityNotFound : ServiceResponse
    {
        public ServiceEntityNotFound()
        {
            Success = false;
            UserMessage = "Requested entity not found, operation could not be performed";
        }
    }

    public class ServiceEntityDuplicated : ServiceResponse
    {
        public ServiceEntityDuplicated(string entityName)
        {
            Success = false;
            UserMessage = "The object that you are trying to add is already in the database.";
        }
    }

    public class ServiceEntityErrorForeignKey : ServiceResponse
    {
        public ServiceEntityErrorForeignKey(string entityName)
        {
            Success = false;
            UserMessage = "There are references to another data source, operation could not finish";
        }
    }

    public class ServiceOperationFailed : ServiceResponse
    {
        string EntityName { get; set; }
        string OperationName { get; set; }
        DateTime OperationFailedDate { get; set; }
        string InnerException { get; }
        private string Layer { get; }
        private int LineNumber { get; set; }
        public ServiceOperationFailed()
        {

        }

        public ServiceOperationFailed(string userMessage)
        {
            UserMessage = userMessage;
        }

        public ServiceOperationFailed(string userMessage, Exception exception, string serviceName = "", string operationName = "")
        {
            OperationFailedDate = DateTime.Now;
            Success = false;
            MessageType = "danger";
            UserMessage = userMessage;
            EntityName = serviceName;
            OperationName = operationName;
            SystemErrorMessage = exception.Message;
            //Si no se especifica la capa, por default es en la capa de servicios
            Layer = "service";
            if (exception.InnerException != null)
                InnerException = exception.InnerException.Message;
            LineNumber = exception.GetLineNumber();
            RegisterFailedOperation();
        }
        void RegisterFailedOperation()
        {
            try
            {
                var webRoot = $"{GlobalVariables.WebRoot}/Logs";
                var fileName = Path.Combine(webRoot, "ErrorLogService.txt");

                if (!Directory.Exists(webRoot))
                    Directory.CreateDirectory(webRoot);

                if (!File.Exists(fileName))
                    using (var streamWriter = new StreamWriter(fileName, true, Encoding.UTF8))
                        streamWriter.WriteLine($"DateTimeError;Entity;Operation;Layer;LineCode;Exception;InnnerException");

                using (var streamWriter = new StreamWriter(fileName, true, Encoding.UTF8))
                    streamWriter.WriteLine($"{OperationFailedDate:yyyy-MM-dd HH:mm:ss};{EntityName};{OperationName};{Layer};{LineNumber};{SystemErrorMessage};{InnerException}");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    //Just for difference between service and controller exception
    public class ServiceException : Exception
    {
        public ServiceException(string userMessage) : base(userMessage)
        {

        }
    }
}
