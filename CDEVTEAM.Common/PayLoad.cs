﻿using System;

namespace CDEVTEAM.Common
{
    public class PayLoad<T>
    {
        public PayLoad()
        {
            Message = "Operation Success";
            Code = (int)HttpResponseCode.Ok;
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
